# encoding: utf-8
from lxml import etree, html
import urllib2, urllib, traceback
from multiprocessing.pool import ThreadPool
import re
import os
sound_path = 'sound'
from task_runner import TaskRunner, SyncTask
import socket
socket.setdefaulttimeout(30)

def read_url(url):
    count = 2 
    data = ""
    while count > 0:
        try:
            response = urllib2.urlopen(url)
            data = response.read()
            break     
        except Exception, e:
            count -= 1 
            if count == 0:
                raise e
    return data           

def download_url(url, file_path):
    count = 2 
    while count > 0:
        try:
            urllib.urlretrieve(url, file_path)
            break
        except Exception, e:
            count -= 1    
            if count == 0:
                raise e

def get_definition(word):
    xml = read_url('http://dict.youdao.com/fsearch?q=%s' % (word))
    xml_obj = etree.fromstring(xml)
    definition_list = []
    for i in xml_obj.findall(".//custom-translation/translation"):
        for j in i.itertext():
            definition_list.append(j)
    return word, definition_list

def fetch_sound_id(word):
    try:
        xml = read_url('http://www.merriam-webster.com/dictionary/%s' % (word))
        html_obj = html.fromstring(xml)
        sound_id_list = []
        for i in html_obj.xpath('.//input[@class="au"]'):
            sound_str = re.findall(r'au\((.+)\);', i.get('onclick'))[0]
            sound_id , token, sound_word = sound_str.partition(",")
            if sound_word.strip('\' ') == word:
                sound_id_list.append(sound_id.strip('\''))
        return sound_id_list
    except Exception, e:
        return []

def download_sound(word):
    global sound_path
    sound_id = fetch_sound_id(word)
    if len(sound_id) == 0:
        return word, 0
    prefix = sound_id[0][0]
    for i, v in enumerate(sound_id):
        download_url("http://media.merriam-webster.com/soundc11/%s/%s.wav" % (prefix, v),
                     "%s/%s_%s.wav" % (sound_path, word, i))
    return word, len(sound_id)

def get_popularity(word):
    xml = read_url('http://www.collinsdictionary.com/dictionary/american/%s' % (word))
    html_obj = html.fromstring(xml)
    i = html_obj.xpath('.//img[@class="commonness_image"]')
    popularity = int(i[0].get('data-band'))
    
    pron = []
    i = html_obj.xpath('.//span[@class="pron"]')
    for j in i:
        pron_elem = j.text.replace('(', '')
        if pron_elem not in pron:
            pron.append(pron_elem)
            
            
    i = html_obj.xpath('.//div[@id="examples_box"]//blockquote')
    example = [j.text for j in i]
    return word, popularity, "".join(pron), example

class WordDownloadTask(SyncTask):
    def is_finish(self):
        return self._wait_for_finish.is_set()
    
    def run(self, word):
        try:
            word, definition = get_definition(word)
            word, popularity, pron, example = get_popularity(word)
            word, sound_count = download_sound(word)
            return word, definition, sound_count, popularity, pron, example
        except Exception, e:
            return word , e

