#!/usr/bin/env python
# encoding: utf-8
import sqlite3, argparse, sys, time, traceback
import ujson as json
from progressbar import ProgressBar, Percentage, Bar, ETA, SimpleProgress
from word_download import TaskRunner, WordDownloadTask
from scikits.audiolab import Sndfile, play

from nltk.tokenize import word_tokenize, wordpunct_tokenize, sent_tokenize
from nltk.stem.lancaster import LancasterStemmer
import re
from random import shuffle

class DataAccess(object):
    def __init__(self, db_name="db/data"):
        self.conn = sqlite3.connect(db_name)
        
    def create_db(self):
        '''
        CREATE TABLE word (
            "word" TEXT NOT NULL PRIMARY KEY AUTOINCREMENT,
            "weight" INTEGER NOT NULL,
            "def_zh" TEXT,
            "def_en" TEXT,
            "popularity" INTEGER,
            "sound_count" INTEGER,
            "example" TEXT,
            "pron" TEXT
        )
        CREATE UNIQUE INDEX "index_word" on word (word ASC)
        '''
        pass
        
    def add_word(self, word, weight, def_zh, def_en, popularity, sound_count, pron, example):
        cursor = self.conn.cursor()
        cursor.execute('INSERT INTO word (word, weight, def_zh, def_en, popularity, sound_count, "pron", example) \
                        VALUES (?,?,?,?,?,?,?,?)', (word, weight, def_zh, def_en, popularity, sound_count, pron, example))    
      
    def query_all_words(self):
        cursor = self.conn.cursor()
        cursor.execute('SELECT word FROM word')
        return cursor.fetchall()      
        
    def query_words_by_weight(self, weight, count, is_random):
        cursor = self.conn.cursor()
        if is_random == True:
            random_str = 'ORDER BY RANDOM()'
        else:
            random_str = 'ORDER BY popularity DESC'
        cursor.execute('SELECT * FROM word WHERE weight = ? %s LIMIT ?' % (random_str), (weight, count))
        return cursor.fetchall()
        
    def query_words_by_review(self, weight, count, is_random, is_test):
        cursor = self.conn.cursor()
        if is_random :
            random_str = 'ORDER BY RANDOM()'
        else:
            random_str = 'ORDER BY review_date DESC'
        if is_test:
            test_str = 'review_date < ?'
        else:
            test_str = '(review_date < ? OR review_date IS NULL)'
        cursor.execute('SELECT * FROM word WHERE weight = ? AND %s %s LIMIT ?' % (test_str, random_str), (weight, time.time(), count))
        return cursor.fetchall()
        
    def query_word(self, word):
        cursor = self.conn.cursor()
        cursor.execute('SELECT * FROM word WHERE word = ?', (word,))
        return cursor.fetchone()
        
    def query_word_like(self, word):
        cursor = self.conn.cursor()
        cursor.execute('SELECT * FROM word WHERE word LIKE "%s%%" ORDER BY word' % (word))
        return cursor.fetchall()        
        
    def set_word_weight(self, word, weight):
        cursor = self.conn.cursor()
        cursor.execute('UPDATE word SET weight = ? \
                        WHERE word = ?', (weight, word))    

    def set_word_review(self, word, date, cycle):
        cursor = self.conn.cursor()
        cursor.execute('UPDATE word SET review_date = ?, review_cycle = ? \
                        WHERE word = ?', (date, cycle, word))   
    
    def query_word_count(self):
        cursor = self.conn.cursor()
        cursor.execute('SELECT count(*) FROM word')
        return cursor.fetchone()
    
    def query_weight_count(self):
        cursor = self.conn.cursor()
        cursor.execute('SELECT weight,count(*) FROM word GROUP BY weight')
        return cursor.fetchall()
    
    def query_review_count(self):
        cursor = self.conn.cursor()
        cursor.execute('SELECT review_cycle,count(*) FROM word GROUP BY review_cycle')
        return cursor.fetchall()    
    
    def commit(self):
        self.conn.commit()
    
def param_parser():
    parser = argparse.ArgumentParser(description="Word drill")
    
    subparsers = parser.add_subparsers(dest="cmd_name")
    # Sub-command load
    cmd_load = subparsers.add_parser('load', help='Load words list')
    cmd_load_group = cmd_load.add_argument_group()
    cmd_load_group.add_argument("-f", dest="file", type=argparse.FileType('r'), required=True,
                        help="List file path.")
    cmd_load_group.add_argument("-w", dest="weight", type=int, required=True,
                        help="word weight")
    
    # Sub-command drill
    cmd_drill = subparsers.add_parser('drill', help='drill word list')
    cmd_drill_group = cmd_drill.add_argument_group()
    
    cmd_drill_group.add_argument("-c", dest="count", type=int, required=True,
                        help="word count")       
    cmd_drill_group_mutually = cmd_drill_group.add_mutually_exclusive_group(required=True)
    cmd_drill_group_mutually.add_argument("-w", dest="weight", type=int,
                        help="word weight")
    cmd_drill_group_mutually.add_argument("-f", dest="file", type=argparse.FileType('r'),
                        help="word list")
    cmd_drill_group.add_argument("-s", dest="sound", action="store_true",
                        help="play word sound")
    cmd_drill_group.add_argument("-r", dest="random", action="store_true",
                        help="random word")    
    cmd_drill_group.add_argument("--strict", dest="strict", action="store_true",
                        help="if word test not pass, reduce weight")     
    
    # Sub-command report
    cmd_report = subparsers.add_parser('report', help='Report your learning status')
    cmd_report_group = cmd_report.add_argument_group()
    
    # Sub-command list
    cmd_list = subparsers.add_parser('list', help='list word')
    cmd_list_group = cmd_list.add_argument_group()    

    cmd_list_group.add_argument("-c", dest="count", type=int, required=True,
                        help="word count")       
    cmd_list_group.add_argument("-w", dest="weight", type=int, required=True,
                        help="word weight")
    cmd_list_group.add_argument("-r", dest="random", action="store_true",
                        help="random word")        
    
    # Sub-command learn
    cmd_learn = subparsers.add_parser('learn', help='learn word')
    cmd_learn_group = cmd_learn.add_argument_group()    
    cmd_learn_group_mutually = cmd_learn_group.add_mutually_exclusive_group(required=True)
    cmd_learn_group_mutually.add_argument("-w", dest="weight", type=int,
                        help="word weight")
    cmd_learn_group_mutually.add_argument("-f", dest="file", type=argparse.FileType('r'),
                        help="word list")
    cmd_learn_group.add_argument("-c", dest="count", type=int, help="word count")        
    cmd_learn_group.add_argument("-s", dest="sound", action="store_true",
                        help="play word sound")
    cmd_learn_group.add_argument("-r", dest="random", action="store_true",
                        help="random word")    
    cmd_learn_group.add_argument("-t", dest="test", action="store_true",
                        help="test word")     
    
    # Sub-command extract
    cmd_extract = subparsers.add_parser('extract', help='extract word from text')
    cmd_extract_group = cmd_extract.add_argument_group()    
    cmd_extract_group.add_argument("-f", dest="file", type=argparse.FileType('r'), required=True,
                        help="List file path.")
    cmd_extract_group.add_argument("-w", dest="weight", type=int,
                        help="weight")      
    
    # Sub-command find
    cmd_find = subparsers.add_parser('find', help='find word')
    cmd_find_group = cmd_find.add_argument_group()    
    cmd_find_group.add_argument("-t", dest="text", type=str, required=True,
                        help="word")
 
    # Sub-command weight
    cmd_weight = subparsers.add_parser('weight', help='set word weight')
    cmd_weight_group = cmd_weight.add_argument_group()    
    cmd_weight_group.add_argument("-t", dest="word", type=str, required=True,
                        help="word")  
    cmd_weight_group.add_argument("-w", dest="weight", type=int, required=True,
                        help="weight")   
    return parser.parse_args()    
    
def word_load(list_file, weight):
    db = DataAccess()
    content = list_file.read().split("\n")
    task_runner = TaskRunner(10)
    task_runner.start()
    word_task = []
    word_failed = []
    for line in content:
        striped_line = line.strip()
        if len(striped_line) != 0:
            try:
                if db.query_word(striped_line) == None:
                    t = WordDownloadTask(striped_line)
                    task_runner.add_task(t)
                    word_task.append(t)
            except Exception, e:
                print "%s" % (striped_line)
                continue
    if len(word_task) == 0:
        task_runner.join()
        return
    pbar = ProgressBar(widgets=[Percentage(), " ", ETA(), Bar(), SimpleProgress()], maxval=len(word_task)).start()
    i = 0 
    while len(word_task) != 0:
        time.sleep(1)
        for task in word_task:
            if task.is_finish() == True:
                try:
                    rt = task.get_result()
                    if len(rt) != 6:
                        word, e = rt
                        raise e
                    word, definition, sound_count, popularity, pron, example = rt 
                    db.add_word(word, weight, json.dumps(definition), "", popularity, sound_count, pron, json.dumps(example))
                    db.commit()
                    word_task.remove(task)
                except Exception, e:
                    word_task.remove(task)
                    word_failed.append("%s %s" % (word, e))
                pbar.update(i)
                i += 1  
    db.commit()
    pbar.finish()
    task_runner.join()
    print "Failed words:"
    print "\n".join(word_failed)
    
def word_drill(content, weight, count, is_sound, is_random, is_strict):
    db = DataAccess()
    word_list = []
    if weight != None:
        if count == None:
            count = -1  
        word_list = db.query_words_by_weight(weight, count, is_random)
    else:
        for i in content.read().split("\n"):
            rt = db.query_word(i.strip())
            if rt != None:
                word_list.append(rt)
        if is_random:
            shuffle(word_list) 
    for i in word_list:
        word, weight, def_zh, def_en, popularity, sound_count, example, pron, review_date, review_cycle = i
        if word_test(i, is_sound):
            db.set_word_weight(word, weight + 1)
            db.commit()
        else:
            if is_strict:
                db.set_word_weight(word, weight - 1)
                db.commit()
    db.commit()
    
def word_report():
    db = DataAccess()
    total = db.query_word_count()[0]
    print "total:", total
    print "weight", "\t", "count", "\t", "%"
    for weight, count in db.query_weight_count():
        print weight, "\t", count, "\t", "%.3f%%" % (count / float(total) * 100)
    print ''
    print "cycle", "\t", "count", "\t", "%"
    for review_cycle, count in db.query_review_count():
        print review_cycle, "\t", count, "\t", "%.3f%%" % (count / float(total) * 100)        
    
def word_list(weight, count, is_random):
    db = DataAccess()
    for i in db.query_words_by_weight(weight, count, is_random):
        print i[0]

def word_learn(content, weight, count, is_sound, is_random, is_test):
    db = DataAccess()
    word_list = []
    if weight != None:
        if count == None:
            count = -1
        word_list = db.query_words_by_review(weight, count, is_random, is_test)
    else:
        for i in content.read().split("\n"):
            rt = db.query_word(i.strip())
            if rt != None:
                word_list.append(rt)
        if is_random:
            shuffle(word_list)
    if not is_test:
        for i in word_list:
            word_print(i, True, True)   
            raw_input("")
    while len(word_list) != 0:
        for i in word_list:
            if word_test(i, is_sound):
                word_review(db, i)
                word_list.remove(i)
        db.commit()
        
def word_extract(content, weight):
    db = DataAccess()
    word_set = set()
    result_set = set()
    for i in sent_tokenize(content):
        word_tokens = wordpunct_tokenize(i)
        word_tokens = filter(lambda word: not re.match(r'\w.[A-Z]+', word), word_tokens)
        word_tokens[0].lower()
        word_tokens = filter(lambda word: re.match(r'[a-z]+', word), word_tokens)
        word_set.update(set(list(word_tokens)))
    if weight != None:
        known_word_list = set([j[0].encode('latin-1') for j in db.query_words_by_weight(weight, -1, False)])
        result_set = known_word_list & word_set
    else:
        known_word_list = set([j[0].encode('latin-1') for j in db.query_all_words()])
        stemmer = LancasterStemmer()
        known_stems = [stemmer.stem(k) for k in known_word_list]
        for j in word_set:
            if stemmer.stem(j) not in known_stems:
                result_set.add(j)
    print "\n".join(result_set)
    
def word_find(word):
    db = DataAccess()
    rt = db.query_word_like(word)
    if len(rt) != 0:
        for j in rt:
            word_print(j, True, True)   

def word_set_weight(word, weight):
    db = DataAccess()
    rt = db.set_word_weight(word, weight)
    db.set_word_review(word, None, None)
    db.commit()

def word_review(db, word_obj):
    review_seq = [0.00347, 0.02083, 0.5, 1, 2, 4, 7, 15, 30]
    word = word_obj[0]
    review_cycle = word_obj[9]
    if review_cycle == None or review_cycle == "":
        review_cycle = 0
    elif review_cycle == 8:
        review_cycle = 8
    else:
        review_cycle += 1
    review_date = int(time.time() + review_seq[review_cycle] * 3600 * 24)
    db.set_word_review(word, review_date, review_cycle)
    
def play_sound(file_path):
    try:
        wave_file = Sndfile(file_path, 'r')
        signal = wave_file.read_frames(wave_file.nframes)
        old_fd = sys.stdout
        while  True:
            try:
                sys.stdout = open("/dev/null", 'w')
                play(signal[:], wave_file.samplerate)
                break
            except Exception, e:
                print(e)
                continue
        sys.stdout = old_fd
    except Exception, e:
        print e
        return

def play_word_sound(word_name, count):
    for i in range(count):
        # print("sound/%s_%s.wav" % (word_name, i))
        play_sound("sound/%s_%s.wav" % (word_name, i))
        if count != 1:
            time.sleep(0.1)    
    
def word_test(word_obj, is_sound):
    word = word_obj[0]
    sound_count = word_obj[5]
    test_ok = False
    while True:
        if is_sound:
            play_word_sound(word, sound_count)
        rt = raw_input("\n\033[1m%s\033[0m remember?(y/n)" % (word))
        if rt == "y":
            test_ok = True
            break
        elif rt == "n":
            test_ok = False
            break
        elif rt == "":
            word_print(word_obj, True, True)
        else:
            continue   
    word_print(word_obj, False, False)
    return test_ok
    
def word_print(word_obj, is_sound, is_example):
    word, weight, def_zh, def_en, popularity, sound_count, example, pron, review_date, review_cycle = word_obj
    print word, "\t", "/", word_pron_convert(pron), "/", "\tpopularity", popularity, "\tweight", weight, "\treview_date", review_date
    if is_sound:
        play_word_sound(word, sound_count)
    print '\n'.join(json.loads(def_zh))
    if is_example:
        print '\n'.join(json.loads(example))    
    
def word_pron_convert(pron):
    try:
        return pron.encode('latin-1')
    except Exception, e:
        return pron.encode('utf-8')

if __name__ == '__main__':
    args = param_parser()
    if args.cmd_name == "load":
        word_load(args.file, args.weight)
    elif args.cmd_name == "drill":
        word_drill(args.file, args.weight, args.count, args.sound, args.random, args.strict)
    elif args.cmd_name == "report":
        word_report()
    elif args.cmd_name == "list":
        word_list(args.weight, args.count, args.random)
    elif args.cmd_name == "learn":
        word_learn(args.file, args.weight, args.count, args.sound, args.random, args.test)  
    elif args.cmd_name == "extract":
        word_extract(args.file.read(), args.weight)          
    elif args.cmd_name == "find":
        word_find(args.text)  
    elif args.cmd_name == "weight":
        word_set_weight(args.word, args.weight)  
